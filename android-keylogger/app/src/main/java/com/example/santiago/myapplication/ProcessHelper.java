package com.example.santiago.myapplication;

/**
 * Created by santiago on 6/13/17.
 */

import android.app.ActivityManager;

public class ProcessHelper {

    private ActivityManager _activityManager;

    public ProcessHelper(ActivityManager activityManager) {
        _activityManager = activityManager;
    }

    public String getForegroundApp() {

        ActivityManager.RunningTaskInfo foregroundTaskInfo = _activityManager.getRunningTasks(1).get(0);
        return foregroundTaskInfo.topActivity.getPackageName();

    }

}
