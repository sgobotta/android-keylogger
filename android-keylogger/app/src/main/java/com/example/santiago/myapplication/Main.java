package com.example.santiago.myapplication;

/**
 * Created by santiago on 6/13/17.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;

public class Main extends Activity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //startActivity(new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION));
        Intent svc = new Intent(this, OverlayShowingService.class);

        startService(svc);
        System.out.println(this.getFilesDir());
        if (isFinishing()){
            finish();
        }

    }
}
